//
//  EventRow.swift
//  Cours Dim WatchKit Extension
//
//  Created by Erwan Martin on 06/12/2021.
//

import SwiftUI

struct EventRow: View {
    var ev:CalendarEvent
    
    @Environment(\.isLuminanceReduced) var isLuminanceReduced
    
    var body: some View {
        VStack(alignment: .leading){
            Text(
                ev.start + " - " + ev.end
            )
            .font(.headline)
            .fontWeight(.light)
            .padding(.all, 8.0)
            .frame(
                minWidth: 0,
                maxWidth: .infinity,
                alignment: .leading
            )
            .background(isLuminanceReduced ? Color.white.opacity(0.1) : Color.red)
            
            HStack(alignment: .top) {
                Divider()
                    .frame(width: isLuminanceReduced ? 0.0 : 4.0, height: 30)
                    .background(Color.purple)
                    .cornerRadius(4)
                    .offset(y: 4)
                
                Text(
                    ev.summary
                )
                .font(.headline)
                .fontWeight(.light)
                .foregroundColor(isLuminanceReduced ? Color.white.opacity(0.8) : Color.black)
                
            }
            .padding([.leading, .bottom])
        }
        .frame(
            minWidth: 0,
            maxWidth: .infinity
        )
        .background(isLuminanceReduced ? Color.black :  Color.white)
        .cornerRadius(10)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color.white.opacity(0.3), lineWidth: isLuminanceReduced ? 2 : 0)
        )
    }
}

struct EventRow_Previews: PreviewProvider {
    static var previews: some View {
        EventRow(ev:CalendarEvent(date:"07-12-2021", day:"Lundi", start:"08:30", end:"12:00", summary:"DIM - ProjetCo - Suivi Projet_IUT", location:"D251", description:"\\n\\nLPDIM\\nMASLOVA OLGA\\n(Export\\u00e9 le"))
    }
}
