//
//  edtApp.swift
//  edt WatchKit Extension
//
//  Created by Erwan Martin on 14/12/2021.
//

import SwiftUI

@main
struct edtApp: App {
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
            }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
