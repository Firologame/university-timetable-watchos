//
//  DayView.swift
//  Cours Dim WatchKit Extension
//
//  Created by Erwan Martin on 12/12/2021.
//

import SwiftUI

struct DayView: View {
    @State public var transitionDirection: String = "right"
    var day: Day
    
    @State private var opacity = 1.0
    @State private var offsetX = 0.0
    
    var body: some View {
        VStack {
            if(day.eventList.count != 0) {
                ScrollView(showsIndicators: false) {
                    Spacer().frame(height: 5)
                    ForEach(day.eventList) { evnt in
                        NavigationLink(destination: EventDetail(ev: evnt)) {
                            EventRow(ev: evnt)
                        }.buttonStyle(PlainButtonStyle()).padding(.top, 5.0)
                    }
                    Spacer().frame(height: 10)
                }.opacity(opacity).offset(x: offsetX).frame(
                    maxWidth: .infinity
                )
            }
            else {
                VStack {
                    Spacer()
                    Text("Pas de cours")
                        .font(.subheadline)
                        .multilineTextAlignment(.center)
                    Spacer()
                }.opacity(opacity).offset(x: offsetX).frame(
                    maxWidth: .infinity
                )
            }
        }.onChange(of: day.inTransitionDirection, perform: { newInTransitionDirection in
            if(newInTransitionDirection != "unset") {
                offsetX = (newInTransitionDirection == "left" ? -200.0 : 200.0)
                opacity = 0.0
                
                withAnimation(.easeIn(duration: 0.15)) {
                    opacity = 1.0
                    offsetX = 0.0
                }
            }
        }).onChange(of: day.outTransitionDirection, perform: { newOutTransitionDirection in
            if(newOutTransitionDirection != "unset") {
                opacity = 1.0
                offsetX = 0.0
                
                withAnimation(.easeOut(duration: 0.15)) {
                    offsetX = (newOutTransitionDirection == "left" ? -200.0 : 200.0)
                    opacity = 1.0
                }
            }
        })
    }
}

struct DayView_Previews: PreviewProvider {
    static var previews: some View {
        DayView(
            day: Day(
                textDate: "12-07-2021",
                textDay: "Lundi",
                eventList: [
                    CalendarEvent(date:"07-12-2021", day:"Lundi", start:"08:30", end:"12:00", summary:"DIM - ProjetCo - Suivi Projet_IUT", location:"D251", description:"\\n\\nLPDIM\\nMASLOVA OLGA\\n(Export\\u00e9 le"),
                    CalendarEvent(date:"07-12-2021", day:"Lundi", start:"13:00", end:"16:30", summary:"DIM - ProjetCo - Suivi Projet_IUT", location:"D251", description:"\\n\\nLPDIM\\nMASLOVA OLGA\\n(Export\\u00e9 le")
                ]
            )
        );
    }
}
