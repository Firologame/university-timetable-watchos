//
//  ShortcutComplicationProvider.swift
//  edt WatchKit Extension
//
//  Created by Erwan Martin on 14/12/2021.
//

import Foundation
import ClockKit
import SwiftUI

final class ShortcutComplicationProvider {
    func getTemplateGraphicCornerCircularView() -> CLKComplicationTemplate {
        return CLKComplicationTemplateGraphicCornerCircularView(
            GraphicCornerCircularView(evnt: EventFetcher())
        )
    }
}
