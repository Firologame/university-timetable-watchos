//
//  ShortcutComplication.swift
//  edt WatchKit Extension
//
//  Created by Erwan Martin on 14/12/2021.
//

import SwiftUI
import ClockKit

struct GraphicCornerCircularView: View {
    var evnt:EventFetcher
    
    var body: some View {
        ZStack {
            let length = evnt.today.eventList.count
            
            if(length > 0) {
                Text(length.formatted() + " cours")
                    .multilineTextAlignment(.center)
            }
            else {
                Text("pas cours")
                    .multilineTextAlignment(.center)
            }
        }
    }
}

struct ShortcutComplication_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            GraphicCornerCircularView(evnt: EventFetcher())
            CLKComplicationTemplateGraphicCornerCircularView(
                GraphicCornerCircularView(evnt: EventFetcher())
            ).previewContext()
        }
    }
}
