//
//  ContentView.swift
//  edt WatchKit Extension
//
//  Created by Erwan Martin on 14/12/2021.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var eventManager = EventFetcher()
    @State private var isLoading = false
    
    var body: some View {
        
        VStack(alignment: .center) {
            if(self.eventManager.fetchError == true) {
                Text("Il y a eu une erreur de réseau")
                    .multilineTextAlignment(.center)
                Button(
                    action:{
                        self.eventManager.fetchEvent()
                    })
                {
                    Text("Réesayer")
                }
            }
            else if(!self.eventManager.isLoading) {
                
                DayView(day: self.eventManager.today)
                .overlay(alignment: .bottom) {
                    BottomBarView(eventManager: eventManager)
                }
            }
            else {
                ProgressView()
            }
        }.frame(
            maxWidth: .infinity
        ).navigationTitle(
            Text(
                self.eventManager.fetchError || self.eventManager.isLoading ? "" : self.eventManager.today.textDay.prefix(3) + ". " + self.eventManager.today.textDate
            )
        ).navigationBarTitleDisplayMode(.inline)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
