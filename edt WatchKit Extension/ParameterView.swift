//
//  ParameterView.swift
//  Cours Dim WatchKit Extension
//
//  Created by Erwan Martin on 12/12/2021.
//

import SwiftUI

struct ParameterView: View {
    
    @Environment(\.isLuminanceReduced) var isLuminanceReduced
    
    @State private var link: String = "";
    @State private var blink = 0.0
    @State private var sendKeyboardLetter = "none"
    
    var body: some View {
        VStack {
            HStack {
                ScrollView(.horizontal) {
                    ScrollViewReader { scrollView in
                        HStack {
                            
                            Text(link)
                            .lineLimit(1)
                            .id("start")
                            .padding(.horizontal, 5)
                            
                            Divider()
                            .frame(
                                width: 2.0,
                                height: 20
                            )
                            .background(
                                Color.white
                            )
                            .opacity(isLuminanceReduced ? 0 : blink)
                            .id("end")
                            .offset(x:-9)
                            .onAppear {
                                scrollView.scrollTo("end")
                            }
                            .onChange(of: link) { newLink in
                                scrollView.scrollTo("end")
                            }
                        }
                    }
                }
                .frame(height: 30)
                
                if(link.count > 0) {
                    Button {
                        sendKeyboardLetter = "del"
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                            sendKeyboardLetter = "none"
                        }
                    } label: {
                        Image(systemName: "delete.backward")
                        .resizable()
                        .frame(width: 19, height: 19)
                        .tint(Color.white)
                        .padding(.trailing, 5)
                    }
                    .buttonStyle(PlainButtonStyle())
                }
            }
            .background(isLuminanceReduced ? Color.black : Color.white.opacity(0.2))
            .cornerRadius(10)
            .padding(.horizontal, 10)
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                .stroke(
                    Color.white.opacity(0.3),
                    lineWidth: isLuminanceReduced ? 2 : 0
                )
                .padding(.horizontal, 10)
            )
            
            KeyboardView(text: $link, sendLetter: sendKeyboardLetter)
            
        }
        .navigationTitle("Url de l'EDT")
        .onAppear{
            Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) {timer in
                self.blink = self.blink == 1 ? 0 : 1
            }
        }
    }
}

struct ParameterView_Previews: PreviewProvider {
    static var previews: some View {
        ParameterView()
    }
}
