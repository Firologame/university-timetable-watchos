//
//  KeyboardView.swift
//  Cours Dim WatchKit Extension
//
//  Created by Erwan Martin on 13/12/2021.
//

import SwiftUI

struct KeyboardView: View {
    @Binding var text: String
    
    @State private var isShift = false
    @State private var isSpecial = false
    
    @Environment(\.isLuminanceReduced) var isLuminanceReduced
    
    var sendLetter: String = "none"
    
    @State private var letters = [
        "azertyuio",
        "pqsdfghjk",
        "lmwxcvbn"
    ]
    @State private var specials = [
        "123456789",
        "0-/:;()$&",
        ".,?!'@\""
    ]
    
    var body: some View {
        VStack {
            ForEach(isSpecial ? specials : letters, id: \.self) { lineChar in
                let allChars = lineChar.map { String($0) }
                
                HStack {
                    ForEach(allChars, id: \.self) { char in
                        Button {
                            tap(letter: char)
                        } label: {
                            Text(char)
                            .font(.system(size: 25))
                            .frame(
                                maxWidth: .infinity,
                                alignment: .center
                            )
                        }
                        .buttonStyle(PlainButtonStyle())
                    }
                }
            }
            HStack {
                Spacer()
                
                Button {
                    tap(letter: "shift")
                } label: {
                    Image(systemName: isShift ? "shift.fill" : "shift")
                    .resizable()
                    .frame(width: 19, height: 19)
                    .tint(Color.white)
                }
                .buttonStyle(PlainButtonStyle())
                
                Spacer()

                Button {
                    tap(letter: " ")
                } label: {
                    Text("Espace")
                }
                .buttonStyle(PlainButtonStyle())
                
                Spacer()
                
                Button {
                    tap(letter: "special")
                } label: {
                    Text(isSpecial ? "ABC" : "123")
                    .frame(height: 19)
                    .tint(Color.white)
                }
                .buttonStyle(PlainButtonStyle())
                
                Spacer()
                
            }
        }
        .background(isLuminanceReduced ? Color.black : Color.white.opacity(0.2))
        .offset(y:30)
        .onChange(of: sendLetter) { newLetter in
            if(newLetter != "none") {
                tap(letter: newLetter)
            }
        }
    }
    
    public func tap(letter: String) {
        if(letter == "del") {
            if(text.count > 0) {
                text.removeLast()
            }
        }
        else if(letter == "shift") {
            isShift = !isShift

            for i in letters.indices {
                if(isShift) {
                    letters[i] = letters[i].uppercased()
                }
                else {
                    letters[i] = letters[i].lowercased()
                }
            }
        }
        else if(letter == "special") {
            isSpecial = !isSpecial
        }
        else if(letter == "validate") {
            print("validate")
        }
        else {
            text += letter
        }
        
    }
}

struct KeyboardView_Previews: PreviewProvider {
    
    static var previews: some View {
        KeyboardView(text: .constant("test text"))
    }
}
