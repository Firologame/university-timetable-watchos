//
//  EventDetail.swift
//  Cours Dim WatchKit Extension
//
//  Created by Erwan Martin on 06/12/2021.
//

import SwiftUI

struct EventDetail: View {
    @Environment(\.presentationMode)
    var presentationMode: Binding<PresentationMode>
    
    var ev: CalendarEvent

    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                Spacer()
                Text("🕘 " + ev.start + " - " + ev.end)
                    .font(.headline)
                    .fontWeight(.regular)
                    .padding(.bottom, 4.0)
                    .fixedSize(horizontal: false, vertical: true)

                Text("📍 " + ev.location)
                    .font(.headline)
                    .fontWeight(.regular)
                    .padding(.bottom, 4.0)
                    .fixedSize(horizontal: false, vertical: true)
                
                Divider()

                Text(ev.summary)
                    .font(.caption)
                    .fontWeight(.ultraLight)
                    .padding(.top, 4.0)
                    .fixedSize(horizontal: false, vertical: true)

                Text(ev.description)
                    .font(.caption)
                    .fontWeight(.ultraLight)
                    .padding(.top, 4.0)
                    .fixedSize(horizontal: false, vertical: true)
            }
            Spacer()
        }.navigationTitle(
            Text(
                ev.day.prefix(3) + ". " + ev.date
            )
        ).navigationBarTitleDisplayMode(.inline)
    }
}

struct EventDetail_Previews: PreviewProvider {
  static var previews: some View {
    EventDetail(
      ev: CalendarEvent(
        date: "12-07-2001", day: "Lundi", start: "08:30", end: "12:00", summary: "DIM - ProjetCo",
        location: "D251", description: "LPDIM MASLOVA OLGA"))
  }
}
