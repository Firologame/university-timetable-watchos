//
//  NotificationView.swift
//  edt WatchKit Extension
//
//  Created by Erwan Martin on 14/12/2021.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
