//
//  DateTools.swift
//  Cours Dim WatchKit Extension
//
//  Created by Erwan Martin on 11/12/2021.
//

import Foundation

public class DateTools {
    
    class func getToday() -> String {
        let ddate = Date()
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy"
        return df.string(from: ddate)
    }
    
    class func getNextDay(day: String = DateTools.getToday()) -> String {
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy"
        let date = df.date(from: day)
        let currentCalendar = Calendar.current
        let nextDay = currentCalendar.date(byAdding: .day, value: 1, to: date ?? Date()) ?? Date()
        
        return df.string(from: nextDay)
    }
    
    class func getPrevDay(day: String = DateTools.getToday()) -> String {
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy"
        let date = df.date(from: day)
        let currentCalendar = Calendar.current
        let nextDay = currentCalendar.date(byAdding: .day, value: -1, to: date ?? Date()) ?? Date()
        
        return df.string(from: nextDay)
    }
    
    class func getTextDay(day: String = DateTools.getToday()) -> String {
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy"
        let date = df.date(from: day) ?? Date()
        
        df.locale = Locale(identifier: "fr_FR")
        df.dateFormat = "EEEE"
        return df.string(from: date).capitalized
    }
    
    class func minutesBetween(date1:Date, date2:Date) -> Int {
        let calendar = Calendar.current
        let time1Components = calendar.dateComponents([.hour, .minute], from: date1)
        let time2Components = calendar.dateComponents([.hour, .minute], from: date2)
        
        let difference = calendar.dateComponents([.minute], from: time1Components, to: time2Components).minute!
        
        return difference
    }
}
