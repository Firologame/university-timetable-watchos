//
//  EventFetcher.swift
//  Cours Dim WatchKit Extension
//
//  Created by Erwan Martin on 06/12/2021.
//

import Foundation
import SwiftUI
import Alamofire
import SwiftyJSON

public class EventFetcher: ObservableObject{
    
    init() {
        self.fetchEvent()
    }
    
    @Published var fetchError: Bool = false
    @Published var isLoading: Bool = true
    
    @Published var today: Day = Day(textDate: DateTools.getToday(), textDay: DateTools.getTextDay(), eventList: []);
    
    @Published var json: JSON = JSON("")
    
    @Published var url: String = "https://erwan-martin.fr/api-edt-lpdim/"
    
    private func parseJSON(date: String = DateTools.getToday()) {
        let json = self.json
        
        let today = json[date]
        let todayEvnts = today["eventList"].array ?? []
        var eventArray: [CalendarEvent] = []
        for evnt in todayEvnts {
            eventArray.append(CalendarEvent(
                date:today["textDate"].string ?? "00-00-0000",
                day: today["textDay"].string ?? "",
                start: evnt["start"].string ?? "00:00",
                end:evnt["end"].string ?? "00:00",
                summary: evnt["summary"].string ?? "",
                location: evnt["location"].string ?? "",
                description: evnt["description"].string ?? ""
            ))

        }
        self.today = Day(
            textDate: today["textDate"].stringValue,
            textDay: today["textDay"].stringValue,
            eventList: eventArray
        )
    }
    
    func getYesterday() -> Day {
        let yesterday = self.json[DateTools.getPrevDay(day: self.today.textDate)]
        let yesterEvnts = yesterday["eventList"].array ?? []
        var eventArray: [CalendarEvent] = []
        for evnt in yesterEvnts {
            eventArray.append(CalendarEvent(
                date: yesterday["textDate"].string ?? "00-00-0000",
                day: yesterday["textDay"].string ?? "",
                start: evnt["start"].string ?? "00:00",
                end:evnt["end"].string ?? "00:00",
                summary: evnt["summary"].string ?? "",
                location: evnt["location"].string ?? "",
                description: evnt["description"].string ?? ""
            ))

        }
        return Day(
            textDate: yesterday["textDate"].stringValue,
            textDay: yesterday["textDay"].stringValue,
            eventList: eventArray
        )
    }
    
    func getTomorrow() -> Day {
        let tomorrow = self.json[DateTools.getNextDay(day: self.today.textDate)]
        let tomorrowEvnts = tomorrow["eventList"].array ?? []
        var eventArray: [CalendarEvent] = []
        for evnt in tomorrowEvnts {
            eventArray.append(CalendarEvent(
                date: tomorrow["textDate"].string ?? "00-00-0000",
                day: tomorrow["textDay"].string ?? "",
                start: evnt["start"].string ?? "00:00",
                end:evnt["end"].string ?? "00:00",
                summary: evnt["summary"].string ?? "",
                location: evnt["location"].string ?? "",
                description: evnt["description"].string ?? ""
            ))

        }
        return Day(
            textDate: tomorrow["textDate"].stringValue,
            textDay: tomorrow["textDay"].stringValue,
            eventList: eventArray
        )
    }
    
    func fetchEvent(date: String = DateTools.getToday(), doNotUpdate: Bool = false) {
        self.json = JSON(UserDefaults.standard.object(forKey: "icsJSON") ?? JSON(""))
        let icsLastSave = UserDefaults.standard.object(forKey: "icsLastSave") ?? Date(timeIntervalSinceReferenceDate: -123456789.0)
        
        // if json is empty or last json save > 5 minutes
        if(self.json.count == 0 || abs(DateTools.minutesBetween(date1: Date(), date2: icsLastSave as! Date)) > 5) {

            self.fetchError = false
            self.isLoading = true
            
            AF.request(self.url).responseJSON{
                response in switch response.result {
                case .success(let value):
                    
                    UserDefaults.standard.set(value, forKey: "icsJSON")
                    UserDefaults.standard.set(Date(), forKey: "icsLastSave")
                    self.json = JSON(value)
                    self.parseJSON(date: date)
                    
                    self.isLoading = false
                    
                case .failure(let error):
                    print(error)
                    self.isLoading = false
                    self.fetchError = true
                }
            }
        }
        else {
            if(!doNotUpdate) {
                self.parseJSON(date: date)
                self.isLoading = false
            }
        }
    }
}
