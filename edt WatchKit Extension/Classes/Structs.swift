//
//  Structs.swift
//  Cours Dim WatchKit Extension
//
//  Created by Erwan Martin on 11/12/2021.
//

import Foundation

struct CalendarEvent: Hashable, Identifiable {
    public var id: String
    public var date: String
    public var day: String
    public var start: String
    public var end: String
    public var summary: String
    public var location: String
    public var description: String
    
    init(date: String, day: String, start: String, end: String, summary: String, location: String, description: String) {
        self.id = UUID().uuidString;
        self.date = date
        self.day = day
        self.start = start
        self.end = end
        self.description = description
        self.summary = summary
        self.location = location
    }
}

struct Day: Hashable, Identifiable {
    public var id: String
    public var textDate: String
    public var textDay: String
    public var eventList: [CalendarEvent]
    public var inTransitionDirection: String;
    public var outTransitionDirection: String = "unset"
    
    init(textDate: String, textDay: String, eventList: [CalendarEvent], inTransitionDirection: String = "unset") {
        self.id = UUID().uuidString;
        self.textDate = textDate
        self.textDay = textDay
        self.eventList = eventList
        self.inTransitionDirection = inTransitionDirection
    }
}
