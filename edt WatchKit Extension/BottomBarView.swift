//
//  BottomBarView.swift
//  Cours Dim WatchKit Extension
//
//  Created by Erwan Martin on 12/12/2021.
//

import SwiftUI

struct BottomBarView: View {
    @ObservedObject var eventManager : EventFetcher
    @Environment(\.isLuminanceReduced) var isLuminanceReduced
    
    var body: some View {
        HStack {
            
            Button {
                print("paramètres")
            } label: {
                NavigationLink(destination: ParameterView()) {
                    Image(systemName: "gearshape.fill")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .tint(Color.white)
                }.buttonStyle(PlainButtonStyle())
            }
            .buttonStyle(PlainButtonStyle())
            
            
            Spacer()
            Spacer()
            
            
            Button {
                self.eventManager.today.outTransitionDirection = "right"
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
                    self.eventManager.today = self.eventManager.getYesterday()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                        self.eventManager.today.inTransitionDirection = "left"
                        self.eventManager.fetchEvent(date: self.eventManager.today.textDate, doNotUpdate: true)
                    }
                }
            } label: {
                Image(systemName: "arrow.left")
                    .resizable()
                    .frame(width: 20, height: 20)
                    .tint(Color.white)
            }
            .buttonStyle(PlainButtonStyle())
            .disabled(self.eventManager.today.textDate == DateTools.getToday())
            
            
            Spacer()
            
            
            Button {
                self.eventManager.today.outTransitionDirection = "left"
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
                    self.eventManager.today = self.eventManager.getTomorrow()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                        self.eventManager.today.inTransitionDirection = "right"
                        self.eventManager.fetchEvent(date: self.eventManager.today.textDate, doNotUpdate: true)
                    }
                }
            } label: {
                Image(systemName: "arrow.right")
                    .resizable()
                    .frame(width: 20, height: 20)
                    .tint(Color.white)
            }
            .buttonStyle(PlainButtonStyle())
            
            
        }.padding(
            EdgeInsets(top: -15, leading: 15, bottom: 0, trailing: 15)
        ).offset(
            y:30
        ).background(
            Color.black.opacity(0.9)
        ).opacity(isLuminanceReduced ? 0 : 1)
    }
}

struct BottomBarView_Previews: PreviewProvider {
    static var previews: some View {
        BottomBarView(eventManager: EventFetcher())
    }
}
